import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/appController/app_controller.dart';
import 'package:repositoryflutter/data/datasource/app_repository_impl.dart';
import 'package:repositoryflutter/src/routes/app_navigation.dart';

import 'appController/app_binding.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(AppController(appRepositoryInterface: GlobalRepositoryImpl()));
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'FBP',
      initialRoute: AppRoutes.splash,
      getPages: AppPages.pages,
      initialBinding: AppBinding(),
    );
  }
}
