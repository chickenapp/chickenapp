// To parse this JSON data, do
//
//     final product = productFromMap(jsonString);

import 'dart:convert';

import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class Product extends Equatable {
  Product({
    required this.id,
    required this.productImage,
    required this.name,
    required this.description,
    this.quantity = 0,
    required this.price,
    required this.warningStock,
    required this.umName,
    required this.productTypeName,
    required this.createdAt,
    required this.active,
    // this.quantityToCart = 0,
  });

  final String id;
  final String productImage;
  final String name;
  final String description;
  int quantity;
  // double quantityToCart;
  final double price;
  final int warningStock;
  final String umName;
  final String productTypeName;
  final DateTime createdAt;
  final bool active;

  Product copyWith({
    required String id,
    required String productImage,
    required String name,
    required String description,
    required int quantity,
    required double price,
    required int warningStock,
    required String umName,
    required String productTypeName,
    required DateTime createdAt,
    required bool active,
  }) =>
      Product(
        id: id,
        productImage: productImage,
        name: name,
        description: description,
        quantity: quantity,
        // quantityToCart: quantityToCart,
        price: price,
        warningStock: warningStock,
        umName: umName,
        productTypeName: productTypeName,
        createdAt: createdAt,
        active: active,
      );

  factory Product.fromJson(String str) => Product.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Product.fromMap(Map<String, dynamic> json) => Product(
        id: json["id"],
        productImage: json["product_image"],
        name: json["name"],
        description: json["description"],
        quantity: json["quantity"],
        price: json["price"],
        warningStock: json["warning_stock"],
        umName: json["um_name"],
        productTypeName: json["product_type_name"],
        createdAt: DateTime.parse(json["created_at"]),
        active: json["active"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "product_image": productImage,
        "name": name,
        "description": description,
        "quantity": quantity,
        "price": price,
        "warning_stock": warningStock,
        "um_name": umName,
        "product_type_name": productTypeName,
        "created_at": createdAt.toIso8601String(),
        "active": active,
      };

  @override
  List<Object?> get props => [
        id,
        productImage,
        name,
        description,
        quantity,
        price,
        warningStock,
        umName,
        productTypeName,
        createdAt,
        active
      ];
}
