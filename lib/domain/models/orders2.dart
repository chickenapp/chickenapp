// To parse this JSON data, do
//
//     final createOrderBase = createOrderBaseFromMap(jsonString);

import 'package:equatable/equatable.dart';
import 'dart:convert';

class CreateOrderBase extends Equatable {
  const CreateOrderBase({
    required this.entryDate,
    required this.deliveryDate,
    required this.customerUuid,
    required this.detail,
    required this.orderTypeUuid,
    required this.statusUuid,
    required this.total,
  });

  final String entryDate;
  final String deliveryDate;
  final String customerUuid;
  final List<Detail> detail;
  final String orderTypeUuid;
  final String statusUuid;
  final double total;

  CreateOrderBase copyWith({
    required String entryDate,
    required String deliveryDate,
    required String customerUuid,
    required List<Detail> detail,
    required String orderTypeUuid,
    required String statusUuid,
    required double total,
  }) =>
      CreateOrderBase(
        entryDate: entryDate,
        deliveryDate: deliveryDate,
        customerUuid: customerUuid,
        detail: detail,
        orderTypeUuid: orderTypeUuid,
        statusUuid: statusUuid,
        total: total,
      );

  factory CreateOrderBase.fromJson(String str) =>
      CreateOrderBase.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CreateOrderBase.fromMap(Map<String, dynamic> json) => CreateOrderBase(
        entryDate: json["entry_date"],
        deliveryDate: json["delivery_date"],
        customerUuid: json["customer_uuid"],
        detail: List<Detail>.from(json["detail"].map((x) => Detail.fromMap(x))),
        orderTypeUuid: json["order_type_uuid"],
        statusUuid: json["status_uuid"],
        total: json["total"],
      );

  Map<String, dynamic> toMap() => {
        "entry_date": entryDate,
        "delivery_date": deliveryDate,
        "customer_uuid": customerUuid,
        "detail": List<dynamic>.from(detail.map((x) => x.toMap())),
        "order_type_uuid": orderTypeUuid,
        "status_uuid": statusUuid,
        "total": total,
      };

  @override
  List<Object?> get props => [
        entryDate,
        deliveryDate,
        customerUuid,
        detail,
        orderTypeUuid,
        statusUuid,
        total,
      ];

  factory CreateOrderBase.getNewOrderNew() {
    return const CreateOrderBase(
      entryDate: "entryDate",
      deliveryDate: "deliveryDate",
      customerUuid: "customerUuid",
      detail: [],
      orderTypeUuid: "orderTypeUuid",
      statusUuid: "statusUuid",
      total: 0,
    );
  }
}

class Detail {
  Detail({
    required this.productUuid,
    required this.price,
    required this.quantity,
  });

  final String productUuid;
  final double price;
  final int quantity;

  Detail copyWith({
    required String productUuid,
    required double price,
    required int quantity,
  }) =>
      Detail(
        productUuid: productUuid,
        price: price,
        quantity: quantity,
      );

  factory Detail.fromJson(String str) => Detail.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Detail.fromMap(Map<String, dynamic> json) => Detail(
        productUuid: json["product_uuid"],
        price: json["price"].toDouble(),
        quantity: json["quantity"],
      );

  Map<String, dynamic> toMap() => {
        "product_uuid": productUuid,
        "price": price,
        "quantity": quantity,
      };
}

// To parse this JSON data, do
//
//     final responseCreateOrder = responseCreateOrderFromMap(jsonString);

// ignore: must_be_immutable
class ResponseOrderCreated extends Equatable {
  ResponseOrderCreated({
    required this.message,
  });

  String message;

  ResponseOrderCreated copyWith({
    required String message,
  }) =>
      ResponseOrderCreated(
        message: message,
      );

  factory ResponseOrderCreated.fromJson(String str) =>
      ResponseOrderCreated.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ResponseOrderCreated.fromMap(Map<String, dynamic> json) =>
      ResponseOrderCreated(
        message: json["message:"],
      );

  Map<String, dynamic> toMap() => {
        "message:": message,
      };

  @override
  List<Object?> get props => [message];
}

class OrderPagination extends Equatable {
  const OrderPagination({
    required this.total,
    required this.page,
    required this.size,
  });

  final int total;
  final int page;
  final int size;

  OrderPagination copyWith({
    required int total,
    required int page,
    required int size,
  }) =>
      OrderPagination(
        total: total,
        page: page,
        size: size,
      );

  factory OrderPagination.fromJson(String str) =>
      OrderPagination.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory OrderPagination.fromMap(Map<String, dynamic> json) => OrderPagination(
        total: json["total"],
        page: json["page"],
        size: json["size"],
      );

  Map<String, dynamic> toMap() => {
        "total": total,
        "page": page,
        "size": size,
      };
  @override
  List<Object?> get props => [
        total,
        page,
        size,
      ];
}
