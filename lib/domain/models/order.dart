// ignore_for_file: non_constant_identifier_names

class Order {
  Order({
    required this.pedidoCabecera,
    required this.pedidoDetalle,
  });
  HeaderOrder pedidoCabecera;
  List<DetailOrder> pedidoDetalle;
}

class HeaderOrder {
  HeaderOrder({
    required this.id,
    required this.fecha_realizado,
    required this.fecha_entrega,
    required this.hora_entrega,
    required this.cliente_uuid,
    required this.nombreCliente,
    required this.telefonoCliente,
    required this.entregado,
    required this.tipo_pedido_uuid,
    required this.cobrado,
    required this.createdAt,
    required this.modifiedAt,
  });

  final String id;
  final DateTime fecha_realizado;
  final DateTime fecha_entrega;
  final DateTime hora_entrega;
  final String cliente_uuid;
  String nombreCliente;
  String telefonoCliente;
  final bool entregado;
  final String tipo_pedido_uuid;
  final bool cobrado;
  final DateTime createdAt;
  final DateTime modifiedAt;
}

class DetailOrder {
  DetailOrder({
    required this.id,
    required this.pedido_cabecera_uuid,
    required this.cliente_uuid,
    required this.precio,
    required this.producto_uuid,
    required this.cantidad,
    required this.imagenProducto,
    required this.tipo_pedido_uuid,
    required this.createdAt,
    required this.modifiedAt,
    required this.activo,
  });

  final String id;
  final String pedido_cabecera_uuid;
  final String cliente_uuid;
  double precio;
  final String producto_uuid;
  final double cantidad;
  final String imagenProducto;
  final String tipo_pedido_uuid;
  final DateTime createdAt;
  final DateTime modifiedAt;
  final bool activo;
  @override
  String toString() {
    return {
      "id": id,
      "pedido_cabecera_uuid": pedido_cabecera_uuid,
      "cliente_uuid": cliente_uuid,
      "precio": precio,
      "producto_uuid": producto_uuid,
      "cantidad": cantidad,
      "tipo_pedido_uuid": tipo_pedido_uuid,
      "createdAt": createdAt,
      "modifiedAt": modifiedAt,
      "activo": activo,
    }.toString();
  }
}

Order getNewOrder() => Order(
      pedidoCabecera: HeaderOrder(
        telefonoCliente: '',
        nombreCliente: '',
        id: "",
        fecha_realizado: DateTime.now(),
        fecha_entrega: DateTime.now(),
        hora_entrega: DateTime.now(),
        cliente_uuid: "cliente_uuid",
        entregado: false,
        tipo_pedido_uuid: "tipo_pedido_uuid",
        cobrado: false,
        createdAt: DateTime.now(),
        modifiedAt: DateTime.now(),
      ),
      pedidoDetalle: [
        DetailOrder(
            id: "",
            pedido_cabecera_uuid: "",
            cliente_uuid: "",
            precio: 0,
            producto_uuid: "",
            cantidad: 0,
            imagenProducto: '',
            tipo_pedido_uuid: "",
            createdAt: DateTime.now(),
            modifiedAt: DateTime.now(),
            activo: true),
      ],
    );
