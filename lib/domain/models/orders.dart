// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import 'dart:convert';

// To parse this JSON data, do
//
//     final baseOrder = GetOrder.fromMap(jsonString);

class GetOrder extends Equatable {
  GetOrder({
    this.id,
    required this.entryDate,
    required this.deliveryDate,
    required this.customerId,
    required this.customerName,
    required this.orderType,
    required this.orderStatus,
    required this.total,
    required this.createdAt,
    required this.active,
  });

  String? id;
  final String entryDate;
  final String deliveryDate;
  final String customerId;
  final String customerName;
  final String orderType;
  final String orderStatus;
  final double total;
  final String createdAt;
  final bool active;

  GetOrder copyWith({
    String? id,
    required String entryDate,
    required String deliveryDate,
    required String customerId,
    required String customerName,
    required String orderType,
    required String orderStatus,
    required double total,
    required String createdAt,
    required bool active,
  }) =>
      GetOrder(
        id: id,
        entryDate: entryDate,
        deliveryDate: deliveryDate,
        customerId: customerId,
        customerName: customerName,
        orderType: orderType,
        orderStatus: orderStatus,
        total: total,
        createdAt: createdAt,
        active: active,
      );

  factory GetOrder.fromJson(String str) => GetOrder.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory GetOrder.fromMap(Map<String, dynamic> json) => GetOrder(
        id: json["id"] ?? '',
        entryDate: json["entry_date"],
        deliveryDate: json["delivery_date"],
        customerId: json["customer_id"],
        customerName: json["customer_name"],
        orderType: json["order_type"],
        orderStatus: json["order_status"],
        total: json["total"].toDouble(),
        createdAt: json["created_at"],
        active: json["active"],
      );

  Map<String, dynamic> toMap() => {
        "id": id ?? '',
        "entry_date": entryDate,
        "delivery_date": deliveryDate,
        "customer_id": customerId,
        "customer_name": customerName,
        "order_type": orderType,
        "order_status": orderStatus,
        "total": total,
        "created_at": createdAt,
        "active": active,
      };
  factory GetOrder.getNewOrderNew() {
    return GetOrder(
      id: "",
      entryDate: "",
      deliveryDate: "",
      customerId: "",
      customerName: "",
      orderType: "",
      orderStatus: "",
      total: 0.0,
      createdAt: "",
      active: true,
    );
  }

  @override
  List<Object?> get props => [
        id,
        entryDate,
        deliveryDate,
        customerId,
        customerName,
        orderType,
        orderStatus,
        total,
        createdAt,
        active,
      ];
}

class Details {
  Details({
    required this.productUuid,
    required this.price,
    required this.quantity,
  });

  final String productUuid;
  final double price;
  final int quantity;

  Details copyWith({
    required String productUuid,
    required double price,
    required int quantity,
  }) =>
      Details(
        productUuid: productUuid,
        price: price,
        quantity: quantity,
      );

  factory Details.fromJson(String str) => Details.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Details.fromMap(Map<String, dynamic> json) => Details(
        productUuid: json["product_uuid"],
        price: json["price"].toDouble(),
        quantity: json["quantity"],
      );

  Map<String, dynamic> toMap() => {
        "product_uuid": productUuid,
        "price": price,
        "quantity": quantity,
      };
}

class CreateOrder extends Equatable {
  const CreateOrder({
    required this.entryDate,
    required this.deliveryDate,
    required this.customerUuid,
    required this.orderTypeUuid,
    required this.statusUuid,
    required this.detail,
  });

  final String entryDate;
  final String deliveryDate;
  final String customerUuid;
  final String orderTypeUuid;
  final String statusUuid;
  final List<Details> detail;

  CreateOrder copyWith({
    required String entryDate,
    required String deliveryDate,
    required String customerId,
    required String orderType,
    required String orderStatus,
    required List<Details> detail,
  }) =>
      CreateOrder(
        entryDate: entryDate,
        deliveryDate: deliveryDate,
        customerUuid: customerId,
        orderTypeUuid: orderType,
        statusUuid: orderStatus,
        detail: detail,
      );

  factory CreateOrder.fromJson(String str) =>
      CreateOrder.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CreateOrder.fromMap(Map<String, dynamic> json) => CreateOrder(
        entryDate: json["entry_date"],
        deliveryDate: json["delivery_date"],
        customerUuid: json["customer_uuid"],
        // customerName: json["customer_name"],
        orderTypeUuid: json["order_type_uuid"],
        statusUuid: json["status_uuid"],
        detail: const [],
      );

  Map<String, dynamic> toMap() => {
        "entry_date": entryDate,
        "delivery_date": deliveryDate,
        "customer_uuid": customerUuid,
        // "customer_name": customerName,
        "order_type_uuid": orderTypeUuid,
        "status_uuid": statusUuid,
        "detail": detail,
      };
  factory CreateOrder.getOrderNew() {
    return const CreateOrder(
        entryDate: "",
        deliveryDate: "",
        customerUuid: "",
        orderTypeUuid: "",
        statusUuid: "",
        detail: []);
  }

  @override
  List<Object?> get props => [
        entryDate,
        deliveryDate,
        customerUuid,
        orderTypeUuid,
        statusUuid,
        detail
      ];
}
