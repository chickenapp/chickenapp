// To parse this JSON data, do
//
//     final product = productFromMap(jsonString);

// ignore_for_file: must_be_immutable

import 'dart:convert';

import 'package:equatable/equatable.dart';

class BaseCustomer extends Equatable {
  BaseCustomer({
    required this.name,
    required this.phone,
  });
  String name;
  String phone;

  BaseCustomer copyWith({
    required String id,
    String? email,
    required bool active,
  }) =>
      BaseCustomer(name: name, phone: phone);

  factory BaseCustomer.fromJson(String str) =>
      BaseCustomer.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BaseCustomer.fromMap(Map<String, dynamic> json) => BaseCustomer(
        name: json["name"],
        phone: json["phone"],
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "phone": phone,
      };
  @override
  List<Object?> get props => [];
  factory BaseCustomer.emptyCustomer() {
    return BaseCustomer(name: '', phone: '');
  }
}

class Customer extends BaseCustomer {
  // ignore: prefer_const_constructors_in_immutables
  Customer({
    name,
    phone,
    required this.id,
    this.email,
    required this.active,
  }) : super(name: name, phone: phone);

  final String id;
  String? email;
  final bool active;

  @override
  Customer copyWith({
    required String id,
    String? email,
    required bool active,
  }) =>
      Customer(
        id: id,
        email: email ?? '',
        active: active,
      );

  factory Customer.fromJson(String str) => Customer.fromMap(json.decode(str));

  @override
  String toJson() => json.encode(toMap());

  factory Customer.fromMap(Map<String, dynamic> json) => Customer(
        id: json["id"] as String,
        name: json["name"] as String,
        phone: json["phone"] as String,
        email: json["email"] as String,
        active: json["active"] as bool,
      );

  @override
  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "phone": phone,
        "email": email ?? '',
        "active": active,
      };

  @override
  List<Object?> get props => [id, name, phone, email, active, active];

  factory Customer.emptyCustomer() {
    return Customer(id: '0', name: '', phone: '', email: '', active: true);
  }
}
