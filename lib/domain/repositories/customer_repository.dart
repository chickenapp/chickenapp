import 'package:repositoryflutter/domain/models/customer.dart';

abstract class CustomerRepositoryInterface {
  Future<List<Customer>?> getAllCustomers(
      {required int page, required int size});
  Future<List<Customer>?> scheduledCustomers();
  Future<Customer?> getCustomerById(String id);
  Future<List<Customer>?> searchCustomerByname(String name);
  Future<List<Customer>?> searchCustomerByPhone(String phone);
  Future<List<Customer>?> searchCustomerByEmail(String email);
  Future<List<Customer>?> getActiveCustomers();
  Future<List<Customer>?> getInactiveUsers();
  Future<String> createCustomer(Map<String, dynamic> user);
  Future<String> updateCustomer(Map<String, dynamic> user);
  Future<String> deleteCustomer(String userId);
}
