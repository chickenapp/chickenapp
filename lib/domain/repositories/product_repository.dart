import 'package:repositoryflutter/domain/models/product.dart';

abstract class ProductRepositoryInterface {
  Future<List<Product>?> getAllProducts();
}
