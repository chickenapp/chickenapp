import 'package:repositoryflutter/domain/models/index.dart';

abstract class OrdersRepositoryInterface {
  Future<List<GetOrder>?> getAllOrders({required int page, required int size});
  Future<List<GetOrder>?> getActiveOrders(
      {required int page, required int size});
  Future<ResponseOrderCreated?> createOrder(CreateOrderBase order);
}
