import 'package:get/get.dart';
import 'package:repositoryflutter/appController/app_controller.dart';
import 'package:repositoryflutter/data/datasource/app_repository_impl.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AppController>(
      () => AppController(
        appRepositoryInterface: GlobalRepositoryImpl(),
      ),
    );
  }
}
