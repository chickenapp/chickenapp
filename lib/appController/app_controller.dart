import 'package:get/get.dart';
import 'package:repositoryflutter/domain/repositories/app_repository.dart';

enum LoadingState { loading, initial }

class AppController extends GetxController {
  final AppRepositoryInterface appRepositoryInterface;

  AppController({required this.appRepositoryInterface});
}
