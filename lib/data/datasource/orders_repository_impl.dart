import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/instance_manager.dart';
import 'package:logger/logger.dart';
import 'package:repositoryflutter/data/dio/dio.dart';
import 'package:repositoryflutter/domain/repositories/orders_repository.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/orders_controller.dart';
import 'package:repositoryflutter/src/config/globals.dart';

import '../../domain/models/index.dart';

class OrdersRepositoryImpl extends OrdersRepositoryInterface {
  final Api dio = Api();
  final Logger _logger = Logger();
  List<GetOrder>? _ordersList = [];
  Map<String, CreateOrderBase>? order = {};

  @override
  Future<List<GetOrder>?> getAllOrders(
      {required int page, required int size}) async {
    _ordersList = [];
    final Response response =
        await dio.dio.get('${EndPoints().orders}?page=$page&size=$size');
    try {
      if (response.statusCode == 200) {
        for (var item in response.data['items']) {
          _ordersList!.add(GetOrder.fromMap(item));
        }
      } else {
        throw Exception('Failed to load orders list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _ordersList;
  }

  @override
  Future<ResponseOrderCreated?> createOrder(CreateOrderBase order) async {
    Map<String, dynamic>? params = order.toMap();
    late ResponseOrderCreated responseBack;
    final Response response = await dio.dio.post(EndPoints().orders,
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        }),
        data: params);
    try {
      if (response.statusCode == 422) {
        _logger.i(response.data);
      }
      if (response.statusCode == 201) {
        responseBack = ResponseOrderCreated.fromMap(response.data);
      } else {
        throw Exception('Failed to create orders');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return responseBack;
  }

  @override
  Future<List<GetOrder>?> getActiveOrders(
      {required int page, required int size}) async {
    _ordersList = [];
    final Response response = await dio.dio
        .post('${EndPoints().ordersHeadersActive}?page=$page&size=$size');
    try {
      if (response.statusCode == 200) {
        final OrdersController ordersController = Get.put(OrdersController());
        final OrderPagination orderPag = OrderPagination(
            page: response.data['page'],
            size: response.data['size'],
            total: response.data['total']);
        ordersController.ordersToProcess.value = orderPag.total;

        for (var item in response.data['items']) {
          _ordersList!.add(GetOrder.fromMap(item));
        }
      } else {
        throw Exception('Failed to load orders list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _ordersList;
  }
}
