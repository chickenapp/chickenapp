import 'dart:io';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:repositoryflutter/data/dio/dio.dart';
import 'package:repositoryflutter/domain/models/customer.dart';
import 'package:repositoryflutter/domain/repositories/customer_repository.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/customers_controller.dart';
import 'package:repositoryflutter/src/config/globals.dart';

class CustomerRepositoryImpl extends CustomerRepositoryInterface {
  final Api dio = Api();
  final Logger _logger = Logger();
  late final List<Customer>? _customerList = [];
  late final List<Customer>? _searchCustomerList = [];
  String locationUserCreated = '';
  Customer? _singleCustomer = Customer.emptyCustomer();
  List<Customer>? customersListAgenda = [];

  @override
  Future<List<Customer>?> getAllCustomers(
      {required int page,
      required int size,
      CustomerController? customerCtrl}) async {
    customersListAgenda = [];
    final Response response =
        await dio.dio.get('${EndPoints().customers}?page=$page&size=$size');
    try {
      if (response.statusCode == 200) {
        if (customerCtrl != null) {
          customerCtrl.totalCustomer(response.data['total']);
          customerCtrl.totalCustomer.refresh();
        }
        for (var item in response.data['items']) {
          customersListAgenda!.add(Customer.fromMap(item));
        }
      } else {
        throw Exception('Failed to load customers list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return customersListAgenda;
  }

  @override
  Future<List<Customer>?> scheduledCustomers() async {
    customersListAgenda = [];
    final Response response = await dio.dio.get(EndPoints().scheduledCustomers);
    try {
      if (response.statusCode == 200) {
        for (var item in response.data) {
          customersListAgenda!.add(Customer.fromMap(item));
        }
      } else {
        throw Exception('Failed to load customers list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return customersListAgenda;
  }

  @override
  Future<Customer?> getCustomerById(String id) async {
    final Response response = await dio.dio.get('${EndPoints().customers}/$id');
    try {
      if (response.statusCode == 200) {
        _singleCustomer = Customer.fromMap(response.data);
      } else {
        throw Exception('Failed to load customer by id');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _singleCustomer;
  }

  @override
  Future<List<Customer>?> searchCustomerByname(String name) async {
    Map<String, dynamic>? params = {"name": name};
    final Response response =
        await dio.dio.post(EndPoints().searchCustomersByName,
            options: Options(headers: {
              HttpHeaders.contentTypeHeader: "application/json",
            }),
            data: params);
    try {
      if (response.statusCode == 200) {
        for (var item in response.data['items']) {
          _searchCustomerList!.add(Customer.fromMap(item));
        }
      } else {
        throw Exception('Failed to search customers list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _searchCustomerList;
  }

  @override
  Future<List<Customer>?> searchCustomerByPhone(String phone) async {
    Map<String, dynamic>? params = {"phone": phone};
    final Response response =
        await dio.dio.post(EndPoints().searchCustomersByPhone,
            options: Options(headers: {
              HttpHeaders.contentTypeHeader: "application/json",
            }),
            data: params);
    try {
      if (response.statusCode == 200) {
        for (var item in response.data['items']) {
          _searchCustomerList!.add(Customer.fromMap(item));
        }
      } else {
        throw Exception('Failed search customers list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _searchCustomerList;
  }

  @override
  Future<List<Customer>?> searchCustomerByEmail(String email) async {
    Map<String, dynamic>? params = {"email": email};
    final Response response =
        await dio.dio.post(EndPoints().searchCustomersByEmail,
            options: Options(headers: {
              HttpHeaders.contentTypeHeader: "application/json",
            }),
            data: params);
    try {
      if (response.statusCode == 200) {
        for (var item in response.data['items']) {
          _searchCustomerList!.add(Customer.fromMap(item));
        }
      } else {
        throw Exception('Failed to search customers list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _searchCustomerList;
  }

  @override
  Future<List<Customer>?> getActiveCustomers() async {
    final Response response =
        await dio.dio.get('${EndPoints().activeCustomers}?page=1&size=10');
    try {
      if (response.statusCode == 200) {
        for (var item in response.data['items']) {
          _customerList!.add(Customer.fromMap(item));
        }
      } else {
        throw Exception('Failed to load active customers list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _customerList;
  }

  @override
  Future<List<Customer>?> getInactiveUsers() async {
    final Response response =
        await dio.dio.get('${EndPoints().inactiveCustomers}?page=1&size=10');
    try {
      if (response.statusCode == 200) {
        for (var item in response.data['items']) {
          _customerList!.add(Customer.fromMap(item));
        }
      } else {
        throw Exception('Failed to inactive customers list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _customerList;
  }

  @override
  Future<String> createCustomer(Map<String, dynamic> user) async {
    Map<String, dynamic>? params = user;
    final Response response = await dio.dio.post(EndPoints().customers,
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        }),
        data: params);
    try {
      if (response.statusCode == 201) {
        locationUserCreated = response.headers.value('location')!;
      } else {
        throw Exception('Failed to create customer');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending post request!');
        _logger.i(e.message);
      }
    }
    return locationUserCreated;
  }

  @override
  Future<String> updateCustomer(Map<String, dynamic> user) async {
    Map<String, dynamic>? params = {
      "name": user['name'],
      "phone": user['phone'],
      "email": user['email'],
      "active": user['active']
    };
    final Response response =
        await dio.dio.put('${EndPoints().customers}/${user["id"]}',
            options: Options(headers: {
              HttpHeaders.contentTypeHeader: "application/json",
            }),
            data: params);
    try {
      if (response.statusCode == 200) {
        locationUserCreated = response.headers.value('location')!;
      } else {
        throw Exception('Failed to update customer');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending post request!');
        _logger.i(e.message);
      }
    }
    return locationUserCreated;
  }

  @override
  Future<String> deleteCustomer(String userId) async {
    final Response response = await dio.dio.delete(
      '${EndPoints().customers}/$userId}',
      options: Options(headers: {
        HttpHeaders.contentTypeHeader: "application/json",
      }),
    );
    try {
      if (response.statusCode == 200) {
        locationUserCreated = response.headers.value('location')!;
      } else {
        throw Exception('Failed to delete customer');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending post request!');
        _logger.i(e.message);
      }
    }
    return locationUserCreated;
  }
}
