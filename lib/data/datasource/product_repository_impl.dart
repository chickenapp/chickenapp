import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:repositoryflutter/data/dio/dio.dart';
import 'package:repositoryflutter/domain/models/index.dart';
import 'package:repositoryflutter/domain/repositories/product_repository.dart';

class ProductRepositoryImpl extends ProductRepositoryInterface {
  final Api dio = Api();
  final Logger _logger = Logger();
  late final List<Product>? _productList = [];

  @override
  Future<List<Product>?> getAllProducts() async {
    final Response response = await dio.dio.get('/products?page=1&size=10');
    try {
      if (response.statusCode == 200) {
        for (var item in response.data['items']) {
          _productList!.add(Product.fromMap(item));
        }
      } else {
        _logger.e('Failed to load products list');
        throw Exception('Failed to load products list');
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _logger.i('Dio error!');
        _logger.i('STATUS: ${e.response?.statusCode}');
        _logger.i('DATA: ${e.response?.data}');
        _logger.i('HEADERS: ${e.response?.headers}');
        _logger.e(e.response);
      } else {
        _logger.i('Error sending request!');
        _logger.i(e.message);
      }
    }
    return _productList;
  }
}
