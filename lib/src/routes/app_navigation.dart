import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/cart/cart_screen.dart';
import 'package:repositoryflutter/presentation/checkout/checkout.dart';
import 'package:repositoryflutter/presentation/checkout/customer/customer_binding.dart';
import 'package:repositoryflutter/presentation/customers-agenda/customers_agenda.dart';
import 'package:repositoryflutter/presentation/history-order/history_screen.dart';
import 'package:repositoryflutter/presentation/home/home_binding.dart';
import 'package:repositoryflutter/presentation/home/home_screen.dart';
import 'package:repositoryflutter/presentation/pay/pay_screen.dart';
import 'package:repositoryflutter/presentation/splash/splash_binding.dart';
import 'package:repositoryflutter/presentation/splash/splash_screen.dart';

class AppRoutes {
  static const String splash = '/splash';
  static const String home = '/home';
  static const String cart = '/cart';
  static const String checkout = '/checkout';
  static const String list = '/list';
  static const String pay = '/pay';
  static const String history = '/history';
  static const String agenda = '/agenda';
}

class AppPages {
  static final pages = [
    GetPage(
      name: AppRoutes.splash,
      page: () => const SplashScreen(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: AppRoutes.home,
      page: () => const HomeScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.cart,
      page: () => const PagesScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.checkout,
      page: () => const CheckoutScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.list,
      page: () => const PagesScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.pay,
      page: () => const PayScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.history,
      page: () => const HistoryScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.agenda,
      page: () => const CustomersContactListScreen(),
      binding: CustomerBinding(),
    ),
  ];
}

class AppScreens {
  static final screens = [const HomeScreen(), const HistoryScreen()];
}
