class Globals {
  String get url => 'http://10.0.2.2:3000';
  String get fakeApi => 'https://jsonplaceholder.typicode.com';
}

class EndPoints extends Globals {
  String get products => '/products';
  String get activeProducts => '/products/actives';
  String get inactiveProducts => '/products/inactives';
  String get customers => '/customers';
  String get scheduledCustomers => '/customers/schedule';
  String get activeCustomers => '/customers/actives';
  String get inactiveCustomers => '/customers/inactives';
  String get searchCustomersByName => '/customers/name';
  String get searchCustomersByPhone => '/customers/phone';
  String get searchCustomersByEmail => '/customers/email';
  String get status => '/status';
  String get activeStatus => '/status/actives';
  String get inactiveStatus => '/status/inactives';
  String get statusSearch => '/status/search';
  String get orders => '/orders';
  String get ordersHeadersActive => '/orders/header/actives';
  String get productType => '/product_type';
  String get orderType => '/order_type';
  String get um => '/units_measure';
}
