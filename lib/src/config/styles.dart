import 'package:flutter/material.dart';

// base color
const Color fuchsia = Color(0xffFD4092);
final Color fuchsia12 = fuchsia.withOpacity(.12);
final Color fuchsia26 = fuchsia.withOpacity(.26);
final Color fuchsia38 = fuchsia.withOpacity(.38);
final Color fuchsia45 = fuchsia.withOpacity(.45);
final Color fuchsia54 = fuchsia.withOpacity(.54);
final Color fuchsia87 = fuchsia.withOpacity(.87);

// announcement colors
const Color purplePlum = Color(0xFF965EFF);
final Color purplePlum12 = purplePlum.withOpacity(.12);
final Color purplePlum26 = purplePlum.withOpacity(.26);
final Color purplePlum38 = purplePlum.withOpacity(.38);
final Color purplePlum45 = purplePlum.withOpacity(.45);
final Color purplePlum54 = purplePlum.withOpacity(.54);
final Color purplePlum87 = purplePlum.withOpacity(.87);

// CTA colors
const Color darkNavy = Color(0xFF032B5C);
final Color darkNavy12 = darkNavy.withOpacity(.12);
final Color darkNavy26 = darkNavy.withOpacity(.26);
final Color darkNavy38 = darkNavy.withOpacity(.38);
final Color darkNavy45 = darkNavy.withOpacity(.45);
final Color darkNavy54 = darkNavy.withOpacity(.54);
final Color darkNavy87 = darkNavy.withOpacity(.87);

// Secondary CTA colors
const Color yellowSunny = Color(0xffFCD755);
final Color yellowSunny12 = yellowSunny.withOpacity(.12);
final Color yellowSunny26 = yellowSunny.withOpacity(.26);
final Color yellowSunny38 = yellowSunny.withOpacity(.38);
final Color yellowSunny45 = yellowSunny.withOpacity(.45);
final Color yellowSunny54 = yellowSunny.withOpacity(.54);
final Color yellowSunny87 = yellowSunny.withOpacity(.87);

// Pure colors
const Color mainBlack = Color(0xff000000);
const Color mainWhite = Color(0xffffffff);

// Status

const Color orangeChicken = Color(0xffff8628);

const Color whitebackdrop = Color(0xfff2eff2);
const Color doneGreen = Color(0xff0dff00);
const Color errorRed = Color(0xffdf0000);
const Color warningOrange = Color(0xffF78932);
const Color greyProductCard = Color(0xfff4f1f4);
const Color greyDescriptionProductCard = Color.fromARGB(255, 170, 169, 170);
const Color pinkProductCard = Color(0xffea4c89);
Color gradientLoginTop = const Color(0xFF55b572);
Color gradientLoginBottom = const Color(0xFF08613f);
Color greenDarkColor = const Color(0xFF02aa39);
Color greenLightColor = const Color(0xFF41c975);
Color greenActiveColor = const Color.fromARGB(255, 2, 70, 28);
final List<Color> themeColors = [
  fuchsia12,
  fuchsia26,
  fuchsia38,
  fuchsia45,
  fuchsia54,
  fuchsia87,
  fuchsia,
  purplePlum12,
  purplePlum26,
  purplePlum38,
  purplePlum45,
  purplePlum54,
  purplePlum87,
  purplePlum,
  darkNavy12,
  darkNavy26,
  darkNavy38,
  darkNavy45,
  darkNavy54,
  darkNavy87,
  darkNavy,
  yellowSunny12,
  yellowSunny26,
  yellowSunny38,
  yellowSunny45,
  yellowSunny54,
  yellowSunny87,
  yellowSunny,
  doneGreen,
  errorRed,
  warningOrange,
  mainWhite,
  mainBlack,
];
