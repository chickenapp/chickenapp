import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';

class BackDropHome extends StatelessWidget {
  const BackDropHome({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final HomeController controller;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => controller.showCart.isTrue
          ? Positioned.fill(
              top: 0,
              left: 0,
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 3.0,
                  sigmaY: 3.0,
                ),
                child: Container(color: Colors.blue.withOpacity(0)),
              ),
            )
          : const SizedBox(),
    );
  }
}
