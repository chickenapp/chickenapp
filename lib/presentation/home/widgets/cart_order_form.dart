import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/presentation/home/widgets/index.dart';

Set<Set<Object>> cartOrderForm(
    {required Size size, required HomeController controller}) {
  return {
    if (controller.pedidosTramite.isNotEmpty)
      {
        controller.sizeSetter(size),
        controller.showCart.value = true,
        Get.bottomSheet(
                ItemListCart(
                  controller: controller,
                  size: size,
                ),
                barrierColor: const Color.fromARGB(128, 0, 0, 0),
                isDismissible: true,
                enableDrag: true,
                isScrollControlled: true,
                elevation: 0,
                ignoreSafeArea: true)
            .then((value) => {
                  controller.nombreClienteErrorMessage.value = '',
                  controller.orderStep.value = 0,
                  controller.showCart.value = false
                })
      },
  };
}

class ItemListCart extends StatelessWidget {
  const ItemListCart({
    Key? key,
    required this.controller,
    required this.size,
  }) : super(key: key);

  final HomeController controller;
  final Size size;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Obx(
        () => Column(
          children: [
            ...stepCheckout(controller: controller, size: size),
          ],
        ),
      ),
      width: double.infinity,
      height: size.height * .6,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black45,
              blurRadius: 7.0, // soften the shadow
              spreadRadius: 2.0, //extend the shadow
              offset: Offset(
                1.0, // Move to right 10  horizontally
                -1.0, // Move to bottom 10 Vertically
              ),
            )
          ]),
    );
  }
}
