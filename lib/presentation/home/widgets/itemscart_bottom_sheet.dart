import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:repositoryflutter/domain/models/index.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/customers_controller.dart';
import 'package:repositoryflutter/presentation/shared/extensions/index.dart';

final CustomerController customerController = Get.put(CustomerController());

List<Widget> itemsBottomSheet(
    {required HomeController controller, required Size size}) {
  List<Widget> itemsColumn = [];
  itemsColumn.add(
    const Padding(
      padding: EdgeInsets.all(20),
      child: Text(
        'Selecciona el tipo de cliente',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
      ),
    ),
  );
  final oldCustomerButton = SizedBox(
    width: size.width * .8,
    height: 40,
    child: Obx(
      () => ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
        onPressed: () {
          if (customerController.giveMeCustomer == '0') {
            Get.toNamed('/agenda');
          } else {
            debugPrint('hay customer asignados');
            debugPrint(
                'hay giveMeCustomer asignado ${customerController.giveMeCustomer.value}');
          }
        },
        child: Text(
          'Cliente registrado ${customerController.totalCustomer.toString()}',
          style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
      ),
    ),
  );
  final newCustomerButton = SizedBox(
    width: size.width * .8,
    height: 40,
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 0,
        primary: Colors.green,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
      onPressed: () {
        if (customerController.giveMeCustomer == '0') {
          customerController.showNewCustomerForm.value =
              !customerController.showNewCustomerForm.value;
        } else {
          debugPrint(
              'hay singleCustomer ${customerController.singleCustomer.value}');
          debugPrint(
              'hay showNewCustomerForm ${customerController.showNewCustomerForm}');
        }
      },
      child: const Text(
        'Nuevo cliente',
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
    ),
  );
  const dividerButtons = SizedBox(height: 10);
  itemsColumn.add(newCustomerButton);
  itemsColumn.add(dividerButtons);
  itemsColumn.add(oldCustomerButton);
  return itemsColumn;
}

List<Widget> myForms({required HomeController controller, required Size size}) {
  const titleForm = Padding(
    padding: EdgeInsets.all(20),
    child: Text(
      'Datos nuevo cliente',
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
    ),
  );
  final nameInput = Padding(
    padding: const EdgeInsets.all(10.0),
    child: TextFormField(
      focusNode: controller.userFocus,
      key: Key(controller.nameKeyForm),
      validator: (value) {
        if (value == null || value.isEmpty || value.length < 2) {
          // print('Please enter some text');
        } else {
          // print('correcto');
        }
        return null;
      },
      cursorColor: Colors.red,
      style: const TextStyle(
        color: Colors.black45,
      ),
      decoration: InputDecoration(
        prefixIcon: const Icon(
          Icons.person,
          color: Colors.black45,
        ),
        hintText: 'Nombre del cliente',
        hintStyle: const TextStyle(color: Colors.black45),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        fillColor: Colors.white,
        filled: true,
        errorText: controller.nombreClienteErrorMessage.isEmpty
            ? null
            : controller.nombreClienteErrorMessage.value,
        errorStyle:
            const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.transparent, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.transparent, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: TextInputType.name,
      onChanged: (value) {
        controller.nombreCliente.value = value;
        Validators().errorFormFieldValid(controller.nombreCliente,
            controller.nombreClienteErrorMessage, controller.nameKeyForm);
      },
      onSaved: (value) {
        controller.nombreCliente.value = value!;
        Validators().errorFormFieldValid(controller.nombreCliente,
            controller.nombreClienteErrorMessage, controller.nameKeyForm);
      },
    ),
  );
  final phoneInput = Padding(
    padding: const EdgeInsets.all(10.0),
    child: TextFormField(
      focusNode: controller.phoneFocus,
      autofocus: false,
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
        MaskTextInputFormatter(
            mask: '#########',
            filter: {"#": RegExp(r'[0-9]')},
            type: MaskAutoCompletionType.eager)
      ],
      key: Key(controller.phoneKeyForm),
      cursorColor: Colors.red,
      style: const TextStyle(
        color: Colors.black,
      ),
      decoration: InputDecoration(
        prefixIcon: const Icon(
          Icons.phone,
          color: Colors.black45,
        ),
        hintText: 'Teléfono del cliente',
        hintStyle: const TextStyle(color: Colors.black45),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        errorText: controller.telefonoClienteErrorMessage.isEmpty
            ? null
            : controller.telefonoClienteErrorMessage.value,
        errorStyle:
            const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        fillColor: Colors.white,
        filled: true,
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.transparent, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.transparent, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: TextInputType.number,
      onChanged: (value) {
        controller.telefonoCliente.value = value.toString();
        Validators().errorFormFieldValid(controller.telefonoCliente,
            controller.telefonoClienteErrorMessage, controller.phoneKeyForm);
      },
      onSaved: (value) {
        controller.telefonoCliente.value = value.toString();
        Validators().errorFormFieldValid(controller.telefonoCliente,
            controller.telefonoClienteErrorMessage, controller.phoneKeyForm);
      },
    ),
  );
  final emailInput = Padding(
    padding: const EdgeInsets.all(10.0),
    child: TextFormField(
      focusNode: controller.emailFocus,
      autofocus: false,
      key: Key(controller.emailKeyForm),
      cursorColor: Colors.red,
      style: const TextStyle(
        color: Colors.black,
      ),
      decoration: InputDecoration(
        prefixIcon: const Icon(
          Icons.email,
          color: Colors.black45,
        ),
        hintText: 'Email del cliente',
        hintStyle: const TextStyle(color: Colors.black45),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        errorText: controller.emailClienteErrorMessage.isEmpty
            ? null
            : controller.emailClienteErrorMessage.value,
        errorStyle:
            const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        fillColor: Colors.white,
        filled: true,
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.transparent, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.transparent, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: TextInputType.emailAddress,
      onChanged: (value) {
        controller.emailCliente.value = value.toString();
        Validators().errorFormFieldValid(controller.emailCliente,
            controller.emailClienteErrorMessage, controller.emailKeyForm);
      },
      onSaved: (value) {
        controller.emailCliente.value = value.toString();
        Validators().errorFormFieldValid(controller.emailCliente,
            controller.emailClienteErrorMessage, controller.emailKeyForm);
      },
    ),
  );
  final proceedCreateCustomerButton = SizedBox(
    width: size.width * .9,
    height: 40,
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 0,
        primary: Colors.blue,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
      onPressed: () async {
        late Map<String, dynamic> user = {
          "name": controller.nombreCliente.value.toString(),
          "phone": controller.telefonoCliente.value.toString()
        };
        if (controller.emailCliente.isNotEmpty) {
          user = {...user, "email": controller.emailCliente.value.toString()};
        }
        final String idCreada = await customerController.createCustomer(user);
        final Rx<Customer?> customer =
            await customerController.getCustomerById(idCreada);
        customerController.recoveryCustomerFromApiToCheckout.value =
            customer.value;
        customerController.recoveryCustomerFromApiToCheckout.refresh();
        customerController.update();
        Get.back();
      },
      child: const Text(
        'Crear cliente y asignar al pedido actual',
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
    ),
  );
  List<Widget> itemForms = [
    titleForm,
    nameInput,
    phoneInput,
    emailInput,
    proceedCreateCustomerButton
  ];
  return itemForms;
}

List<Widget> stepCheckout(
    {required HomeController controller, required Size size}) {
  if (customerController.showNewCustomerForm.value == false) {
    return itemsBottomSheet(controller: controller, size: size);
  } else {
    return myForms(controller: controller, size: size);
  }
}
