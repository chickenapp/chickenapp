import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/data/datasource/product_repository_impl.dart';
import 'package:repositoryflutter/domain/models/index.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/orders_controller.dart';

class HomeController extends GetxController {
  Rx<Customer?> recoveryCustomerFromApiToCheckout =
      Customer.emptyCustomer().obs;
  RxDouble opacityValue = 1.0.obs;
  RxDouble iconwidth = 80.0.obs;
  RxBool resized = false.obs;
  FocusNode userFocus = FocusNode();
  FocusNode phoneFocus = FocusNode();
  FocusNode emailFocus = FocusNode();
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: false,
  );
  final formKey = GlobalKey<FormState>();
  final String nameKeyForm = 'name';
  final String phoneKeyForm = 'phone';
  final String emailKeyForm = 'email';
  final ProductRepositoryImpl _productRepo = ProductRepositoryImpl();
  final OrdersController _ordersController = Get.put(OrdersController());
  RxBool isContactCreated = false.obs;
  RxBool isDeletingProduct = false.obs;
  RxString nombreCliente = ''.obs;
  RxString telefonoCliente = ''.obs;
  RxString emailCliente = ''.obs;
  RxString nombreClienteErrorMessage = ''.obs;
  RxString telefonoClienteErrorMessage = ''.obs;
  RxString emailClienteErrorMessage = ''.obs;
  RxInt itemsInCart = 0.obs;
  RxInt orderStep = 0.obs;
  RxList<Product> pedidosTramite = <Product>[].obs;
  RxList<Product> listado = <Product>[].obs;
  RxBool hasPedido = false.obs;
  RxBool showCart = false.obs;
  RxList<Detail> preparedCartorder = <Detail>[].obs;

  // ignore: prefer_const_constructors
  Size size = Size(0, 0);

  HomeController();

  Future<List<Product>?> getAllProducts() {
    return _productRepo.getAllProducts();
  }

  addProductToCart(Product product) {
    final int indexElement =
        pedidosTramite.indexWhere((Product prod) => prod.id == product.id);
    if (indexElement != -1) {
      pedidosTramite[indexElement].quantity =
          pedidosTramite[indexElement].quantity + 1;
      itemsInCart.value++;
    } else {
      pedidosTramite.add(product);
      pedidosTramite[pedidosTramite
              .indexWhere((Product prod) => prod.id == product.id)]
          .quantity = 1;
      itemsInCart.value++;
    }
    pedidosTramite.refresh();
    listado.refresh();
    itemsInCart.refresh();
    isDeletingProduct.refresh();
    opacityCheck();
  }

  subtractProductToCart(Product product) async {
    final int indexElement =
        pedidosTramite.indexWhere((Product prod) => prod.id == product.id);
    if (pedidosTramite.isNotEmpty && indexElement != -1) {
      pedidosTramite[indexElement].quantity--;

      pedidosTramite.removeWhere((item) => item.quantity <= 0);

      if (itemsInCart.value < 0) {
        itemsInCart.value = 0;
      } else {
        itemsInCart.value--;
      }
      isDeletingProduct.value = true;

      isDeletingProduct.value = false;
      pedidosTramite.refresh();
      listado.refresh();
      itemsInCart.refresh();
      isDeletingProduct.refresh();
      isCartEmpty();
    }
    opacityCheck();
  }

  getProductsInCart() {
    return pedidosTramite;
  }

  clearProductsInCart() async {
    opacityValue(0);
    await Future.delayed(const Duration(milliseconds: 600), () {
      itemsInCart.value = 0;
      itemsInCart.refresh();
    });

    pedidosTramite.clear();
    update();
  }

  clearUserData() {
    isContactCreated.value = false;
    nombreCliente.value = '';
    telefonoCliente.value = '';
    emailCliente.value = '';
  }

  openCloseCart() {
    if (showCart.isTrue) {
      showCart.value = false;
    } else if (hasPedido.isTrue) {
      showCart.value = true;
    }
  }

  cleanCart() {
    openCloseCart();
    listado.clear();
    itemsInCart = 0.obs;
    hasPedido = false.obs;
  }

  tramitarPedido(RxList<Product> listadoProductos) {
    double precioFinal = 0;
    for (var product in listado) {
      precioFinal = precioFinal + (product.quantity * product.price);
    }
    cleanCart();
  }

  getProducts() async {
    getAllProducts().then((value) {
      for (var item in value!) {
        item.quantity = 0;
        listado.add(item);
      }
    });
  }

  @override
  void onInit() {
    opacityCheck();
    getProducts();
    super.onInit();
    pedidosTramite.refresh();
    listado.refresh();
    itemsInCart.refresh();
    isDeletingProduct.refresh();
    update();
  }

  cleanProduct(Product product) {
    pedidosTramite.removeWhere((item) => item.id == product.id);
    itemsInCart.value = itemsInCart.value - product.quantity.toInt();
    pedidosTramite.refresh();
    listado.refresh();
    itemsInCart.refresh();
    isDeletingProduct.refresh();
    isCartEmpty();
  }

  void isCartEmpty() async {
    if (itemsInCart.value == 0) {
      opacityValue(0);
      await Future.delayed(const Duration(milliseconds: 600), () {
        itemsInCart.value = 0;
        itemsInCart.refresh();
      });
      userFocus.unfocus();
      phoneFocus.unfocus();
      emailFocus.unfocus();
      Get.back();
    }
  }

  sizeSetter(Size s) {
    size = s;
  }

  Size sizeGetter() {
    return size;
  }

  initAnimForward() async {
    opacityValue(0.0);
    await Future.delayed(const Duration(milliseconds: 600), () {
      initAnimReverse();
    });
  }

  initAnimReverse() {
    opacityValue(1.0);
  }

  opacityCheck() {
    if (itemsInCart.value == 0) {
      opacityValue(0);
    } else {
      opacityValue(1);
    }
  }

  double getTotalOrder() {
    List<double> totales = [];
    for (var item in pedidosTramite) {
      totales.add(item.price * item.quantity);
    }
    final double sum =
        totales.fold(0, (previous, current) => previous + current);
    return sum.toPrecision(2);
  }

  prepareCartOrder({required String clientId}) {
    final String entryDate = DateTime.now().toIso8601String();
    final String deliveryDate = DateTime.now().toIso8601String();
    const String orderId = '4398be7d-0cd1-477d-b323-4fbfeb42255d';
    const String statusId = '97903b16-0e84-4fa0-8f99-996def5a4a86';
    List<Detail> listadoProds = [];
    for (var producto in pedidosTramite) {
      listadoProds.add(
        Detail(
            productUuid: producto.id,
            price: producto.price,
            quantity: producto.quantity),
      );
    }
    preparedCartorder = listadoProds.obs;
    final CreateOrderBase pedido = CreateOrderBase(
      entryDate: entryDate,
      deliveryDate: deliveryDate,
      customerUuid: clientId,
      detail: preparedCartorder,
      orderTypeUuid: orderId,
      statusUuid: statusId,
      total: getTotalOrder(),
    );
    _ordersController.createOrder(pedido).then((value) async {
      if (value!.message.isNotEmpty) {
        _ordersController.setMessageOrder(value);
      }
    }).then(
      (value) => {
        Future.delayed(const Duration(seconds: 1), () {
          clearProductsInCart();
          Get.snackbar(
            _ordersController.messageOrder,
            'Pedido enviado a cocina',
            icon: const Icon(Icons.wifi, color: Colors.black),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING,
            duration: const Duration(seconds: 6),
          );
        }),
      },
    );
  }
}
