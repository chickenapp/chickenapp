import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/presentation/home/widgets/index.dart';
import 'package:repositoryflutter/presentation/shared/widgets/index.dart';

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        appBar: HeaderApp(controller: controller, size: size),
        backgroundColor: Colors.white,
        body: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Stack(
              children: [
                ProductListHome(controller: controller, size: size),
                BackDropHome(controller: controller),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
