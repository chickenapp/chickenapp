import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/data/datasource/orders_repository_impl.dart';
import 'package:repositoryflutter/domain/models/index.dart';

class OrdersController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final OrdersRepositoryImpl _customerRepo = OrdersRepositoryImpl();
  Rx<ResponseOrderCreated> mensajeOrder = ResponseOrderCreated(message: '').obs;

  RxInt ordersToProcess = 0.obs;
  RxList<GetOrder> listadoOrders = <GetOrder>[].obs;
  Rx<ResponseOrderCreated?> singleOrder = ResponseOrderCreated(message: '').obs;

  String get messageOrder => mensajeOrder.value.message;
  void setMessageOrder(ResponseOrderCreated mensaje) =>
      mensajeOrder.value = mensaje;
  OrdersController();

  Future<List<GetOrder>?> _getAllOrders(
      {required int page, required int size}) {
    return _customerRepo.getAllOrders(page: page, size: size);
  }

  Future<List<GetOrder>?> _getActiveOrders(
      {required int page, required int size}) {
    return _customerRepo.getActiveOrders(page: page, size: size);
  }

  Future<ResponseOrderCreated?> _createOrder(CreateOrderBase order) {
    return _customerRepo.createOrder(order);
  }

  Future<ResponseOrderCreated?> createOrder(CreateOrderBase order) async {
    initOrdersList();
    _createOrder(order).then((orders) {
      singleOrder.value = orders;
    });
    ordersToProcess.value = ordersToProcess.value + 1;
    return singleOrder.value;
  }

  Future<RxList<GetOrder>> getOrders(
      {required int page, required int size}) async {
    initOrdersList();
    _getAllOrders(page: page, size: size).then((orders) {
      for (var order in orders!) {
        listadoOrders.add(order);
      }
    });
    return listadoOrders;
  }

  Future<RxList<GetOrder>> getActiveOrders(
      {required int page, required int size}) async {
    initOrdersList();
    _getActiveOrders(page: page, size: size).then((orders) {
      for (var order in orders!) {
        listadoOrders.add(order);
      }
    });
    return listadoOrders;
  }

  void initOrdersList() {
    if (listadoOrders.isNotEmpty) {
      listadoOrders(<GetOrder>[]);
    }
  }
}
