import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/data/datasource/customer_repository_impl.dart';
import 'package:repositoryflutter/domain/models/customer.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/presentation/home/widgets/index.dart';

class CustomerController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final HomeController homeController = Get.put(HomeController());

  final CustomerRepositoryImpl _customerRepo = CustomerRepositoryImpl();
  RxList<Customer> listadoCustomers = <Customer>[].obs;
  RxInt totalCustomer = 0.obs;
  RxList<Customer> searchListCustomers = <Customer>[].obs;
  RxList<Customer> agendaCustomers = <Customer>[].obs;
  Rx<Customer?> singleCustomer = Customer.emptyCustomer().obs;
  Rx<Customer?> recoveryCustomerFromApiToCheckout =
      Customer.emptyCustomer().obs;
  RxBool showNewCustomerForm = false.obs;
  RxString idCustomerCreated = ''.obs;
  CustomerController();

  Future<List<Customer>?> _getAllCustomers(
      {required int page, required int size}) {
    return _customerRepo.getAllCustomers(
        page: page, size: size, customerCtrl: this);
  }

  Future<List<Customer>?> _getAllCustomersFuture() {
    return _customerRepo.scheduledCustomers();
  }

  Future<List<Customer>?> getAllCustomersFuture() async {
    initCustomersList();
    _getAllCustomersFuture().then((customers) {
      for (var customer in customers!) {
        listadoCustomers.add(customer);
      }
    });
    return listadoCustomers;
  }

  Future<Customer?> _getCustomerById(String id) {
    return _customerRepo.getCustomerById(id);
  }

  Future<List<Customer>?> _searchCustomerByname(String name) {
    return _customerRepo.searchCustomerByname(name);
  }

  Future<List<Customer>?> _searchCustomerByPhone(String phone) {
    return _customerRepo.searchCustomerByPhone(phone);
  }

  Future<List<Customer>?> _searchCustomerByEmail(String email) {
    return _customerRepo.searchCustomerByEmail(email);
  }

  Future<List<Customer>?> _getActiveCustomers() {
    return _customerRepo.getActiveCustomers();
  }

  Future<String> _createCustomer(Map<String, dynamic> user) {
    return _customerRepo.createCustomer(user);
  }

  Future<String> _updateCustomer(Map<String, dynamic> user) {
    return _customerRepo.updateCustomer(user);
  }

  Future<String> _deleteCustomer(String userId) {
    return _customerRepo.deleteCustomer(userId);
  }

  Future<RxList<Customer>> getCustomers(
      {required int page, required int size}) async {
    initCustomersList();
    _getAllCustomers(page: page, size: size).then((customers) {
      for (var customer in customers!) {
        listadoCustomers.add(customer);
      }
    });
    return listadoCustomers;
  }

  void initCustomersList() {
    listadoCustomers(<Customer>[]);
  }

  RxList<Customer> getActiveCustomers() {
    _getActiveCustomers().then((customers) {
      for (var customer in customers!) {
        listadoCustomers.add(customer);
      }
    });
    return listadoCustomers;
  }

  Future<String> createCustomer(Map<String, dynamic> user) async {
    late String userId;
    await _createCustomer(user).then((value) => userId = value);
    return userId;
  }

  Future<String> updateCustomer(Map<String, dynamic> user) async {
    late String userId;
    await _updateCustomer(user).then((value) => userId = value);
    return userId;
  }

  Future<String> deleteCustomer(String userId) async {
    late String idUser;
    await _deleteCustomer(userId).then((value) => idUser = value);
    return idUser;
  }

  Future<RxList<Customer>> searchCustomerByname(String name) async {
    initSearchCustomerList();
    await _searchCustomerByname(name).then((customers) {
      for (var customer in customers!) {
        searchListCustomers.add(customer);
      }
    });
    searchListCustomers.refresh();
    return searchListCustomers;
  }

  Future<RxList<Customer>> searchCustomerByPhone(String phone) async {
    await _searchCustomerByPhone(phone).then((customers) async {
      await initSearchCustomerList();
      searchListCustomers.refresh();
      for (var customer in customers!) {
        searchListCustomers.add(customer);
      }
    });
    return searchListCustomers;
  }

  Future<RxList<Customer>> searchCustomerByEmail(String email) async {
    await _searchCustomerByEmail(email).then((customers) async {
      await initSearchCustomerList();
      searchListCustomers.refresh();
      for (var customer in customers!) {
        searchListCustomers.add(customer);
      }
    });
    return searchListCustomers;
  }

  Future<Rx<Customer?>> getCustomerById(String id) async {
    await _getCustomerById(id).then((customer) => singleCustomer(customer));
    return singleCustomer;
  }

  @override
  void onInit() async {
    getAllCustomersFuture();
    debugPrint('inicia customer controller');
    super.onInit();
  }

  initSearchCustomerList() {
    searchListCustomers = <Customer>[].obs;
    searchListCustomers.refresh();
    update(searchListCustomers);
  }

  loadBottomSheetTypeUser(Size size) {
    Get.bottomSheet(
            ItemListCart(
              controller: homeController,
              size: size,
            ),
            barrierColor: const Color.fromARGB(128, 0, 0, 0),
            isDismissible: true,
            enableDrag: true,
            isScrollControlled: true,
            elevation: 0,
            ignoreSafeArea: true)
        .then((value) => {
              if (recoveryCustomerFromApiToCheckout.value!.id != '0')
                {
                  homeController.prepareCartOrder(
                      clientId: recoveryCustomerFromApiToCheckout.value!.id),
                },
              customerController.showNewCustomerForm.value = false,
              cleanVars(),
              Get.back(),
            });
  }

  get giveMeCustomer => singleCustomer.value?.id;
  bool get statusNewCustomerForm => showNewCustomerForm.value;
  set setStatusNewCustomerForm(bool statusForm) =>
      showNewCustomerForm(statusForm);

  cleanVars() {
    singleCustomer(Customer.emptyCustomer());
    recoveryCustomerFromApiToCheckout(Customer.emptyCustomer());
    showNewCustomerForm(false);
    singleCustomer.refresh();
    recoveryCustomerFromApiToCheckout.refresh();
    showNewCustomerForm.refresh();
    return update();
  }

  clearListadoCustomers() {
    listadoCustomers.clear();
    listadoCustomers.refresh();
    return update();
  }
}
