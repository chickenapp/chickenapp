/// Flutter icons BPF
/// Copyright (C) 2020 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  BPF
///      fonts:
///       - asset: fonts/BPF.ttf
///
///
/// * Font Awesome 4, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL ()
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
/// * Material Design Icons, Copyright (C) Google, Inc
///         Author:    Google
///         License:   Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
///         Homepage:  https://design.google.com/icons/
/// * Entypo, Copyright (C) 2012 by Daniel Bruce
///         Author:    Daniel Bruce
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://www.entypo.com
/// * Linecons, Copyright (C) 2013 by Designmodo
///         Author:    Designmodo for Smashing Magazine
///         License:   CC BY ()
///         Homepage:  http://designmodo.com/linecons-free/
/// * Linearicons Free, Copyright (C) Linearicons.com
///         Author:    Perxis
///         License:   CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
///         Homepage:  https://linearicons.com
/// * Font Awesome 5, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL (https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
/// * MFG Labs, Copyright (C) 2012 by Daniel Bruce
///         Author:    MFG Labs
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://www.mfglabs.com/
/// * Octicons, Copyright (C) 2020 by GitHub Inc.
///         Author:    GitHub
///         License:   MIT (http://opensource.org/licenses/mit-license.php)
///         Homepage:  https://primer.style/octicons/
///
// ignore_for_file: constant_identifier_names

import 'package:flutter/widgets.dart';

class BPF {
  BPF._();

  static const _kFontFam = 'BPF';
  static const _kFontPkg = null;

  static const IconData heart_empty =
      IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData heart =
      IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData done =
      IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bag =
      IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData plus =
      IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData camera =
      IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData picture =
      IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData home =
      IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cog =
      IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData credit_card =
      IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData truck =
      IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData map =
      IconData(0xe834, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData gift =
      IconData(0xe844, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData smile =
      IconData(0xe854, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData checkmark_cicle =
      IconData(0xe87f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData fire =
      IconData(0xf06d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData info_circled_alt =
      IconData(0xf086, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData comment_empty =
      IconData(0xf0e5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData doc_text =
      IconData(0xf0f6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData dollar_sign =
      IconData(0xf155, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cc_visa =
      IconData(0xf1f0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cc_mastercard =
      IconData(0xf1f1, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData search =
      IconData(0xf3c3, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
