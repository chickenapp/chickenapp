import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/src/config/styles.dart';

class HeaderApp extends StatelessWidget implements PreferredSizeWidget {
  const HeaderApp({
    Key? key,
    required this.controller,
    required this.size,
  }) : super(key: key);

  final HomeController controller;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return AppBar(
        actions: [
          SizedBox(
            width: 100,
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: 8,
                  right: 19,
                  child: GestureDetector(
                    onTap: () => Get.toNamed('/checkout'),
                    child: Stack(
                      children: const [
                        Padding(
                          padding: EdgeInsets.only(top: 8, right: 5),
                          child: Icon(
                            IconData(0xf37f, fontFamily: 'MaterialIcons'),
                            size: 30,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  right: 60,
                  top: 1,
                  child: IconButton(
                    icon: const Icon(
                      Icons.list_alt,
                      size: 40,
                    ),
                    onPressed: () => controller.pageController.jumpToPage(1),
                  ),
                ),
                Obx(
                  () => Positioned(
                    top: 5,
                    right: 10,
                    width: 25,
                    height: 25,
                    child: GestureDetector(
                      onTap: () => Get.toNamed('/checkout'),
                      child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 500),
                        opacity: controller.opacityValue.value,
                        curve: Curves.bounceIn,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(50.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Obx(
                  () => Positioned(
                    top: 9,
                    right:
                        controller.itemsInCart.toString().length > 1 ? 15 : 18,
                    child: AnimatedOpacity(
                      duration: const Duration(milliseconds: 500),
                      opacity: controller.opacityValue.value,
                      curve: Curves.bounceIn,
                      child: Text(
                        controller.itemsInCart.toString(),
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
        elevation: 0,
        backgroundColor: gradientLoginTop,
        centerTitle: false,
        title: Row(
          children: [
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: "C",
                    style: TextStyle(color: Colors.white),
                  ),
                  TextSpan(
                    text: '4',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: greenActiveColor),
                  ),
                  const TextSpan(
                    text: 'U',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              style: const TextStyle(fontSize: 30),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 18.0),
              child: Text('Nuevo pedido'),
            )
          ],
        ),
        automaticallyImplyLeading: false);
  }

  @override
  Size get preferredSize => const Size.fromHeight(60.0);
}
