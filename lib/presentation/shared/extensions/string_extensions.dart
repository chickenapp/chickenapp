import 'package:repositoryflutter/presentation/shared/extensions/words_list.dart';

extension StringExtension on String {
  String capitalizet() {
    return "${this[0].toUpperCase()}${substring(1)}";
  }

  String singularPlural(int items) {
    return items > 1
        ? WordList().pluralOf(trim())
        : WordList().singularOf(trim());
  }

  String pluralOf() {
    return WordList().pluralOf(trim());
  }

  String singularOf() {
    return WordList().singularOf(trim());
  }
}
