import 'package:get/get.dart';

class Validators {
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  static bool isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  void errorFormFieldValid(
      RxString valueCampo, RxString campoError, String field) {
    // at any time, we can get the text from _controller.value.text
    final text = valueCampo.value;
    // Note: you can do your own custom validation here
    // Move this logic this outside the widget for more testable code
    if (text.isEmpty) {
      switch (field) {
        case 'name':
          campoError.value = 'El nombre del cliente es obligatorio';
          break;
        case 'phone':
          campoError.value = 'El télefono del cliente es obligatorio';
          break;
        default:
          campoError.value = 'El nombre del cliente es obligatorio';
      }
    } else if (text.length < 2 && field == 'name') {
      campoError.value = 'El nombre debe tener más de 2 carácteres';
    } else if (text.replaceAll('-', '').length < 9 && field == 'phone') {
      campoError.value = 'El teléfono debe tener mínimo 9 digitos';
    } else if (field == 'email' && text.isNotEmpty && !text.isEmail) {
      campoError.value = 'Debes introducir un email válido';
    } else {
      campoError.value = '';
    }
  }
}
