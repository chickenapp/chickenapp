import 'package:intl/intl.dart';

class PDateUtils {
  static String getFormattedDate(String unfformattedDate) {
    DateTime dateTime = DateTime.parse(unfformattedDate).toLocal();
    DateFormat df = DateFormat("dd/MM/yyyy").add_Hm();
    return df.format(dateTime);
  }

  static String getFormattedDateYearShort(String unfformattedDate) {
    DateTime dateTime = DateTime.parse(unfformattedDate).toLocal();
    DateFormat df = DateFormat("dd/MM/yy").add_Hm();
    String date = df.format(dateTime);

    return date;
  }

  static String getFormattedDateDMY(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    DateTime dateTime = dateFormat.parse(dateString);

    return "${dateTime.day} - ${dateTime.month} - ${dateTime.year}";
  }

  static String getFormattedDateDMYwithLetters(String dateString) {
    if (dateString.isEmpty) {
      return "-";
    }
    DateTime dateTime = DateTime.parse(dateString).toLocal();

    String date = DateFormat.yMMMd('es_ES').format(dateTime);

    return date;
  }

  static String getFormattedDateyMMMMd(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateTime dateTime = DateTime.parse(dateString).toLocal();

    String date = DateFormat.yMMMMd('es_ES').format(dateTime);

    return date;
  }

  static String getFormattedDateDMYwithLettersShort(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateTime dateTime = DateTime.parse(dateString).toLocal();

    String date = DateFormat.yMMMd('es_ES').format(dateTime);

    return date.substring(0, date.length - 2).replaceAll(".", "");
  }

  static String getFormattedTime(String dateString) {
    if (dateString.isEmpty) {
      return "-";
    }
    DateTime dateTime = DateTime.parse(dateString).toLocal();
    String formattedTime = DateFormat.Hm().format(dateTime);

    return formattedTime;
  }

  static String getDaysBetweenDateAndToday(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return "-";
    }
    DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    DateTime dateTime = dateFormat.parse(dateString);
    DateTime today = DateTime.now();
    int difference = dateTime.difference(today).inDays;

    return difference == 0
        ? "Ahora"
        : "Hace ${difference * -1} dÃ­as"; //multipli *-1 to convert to positive
  }

  static DateTime getFormattedDateTimeDMY(String dateString) {
    if (dateString.isEmpty) {
      return DateTime.now();
    }
    DateFormat dateFormat = DateFormat('dd-MM-yyyy');
    return dateFormat.parse(dateString);
  }
}
