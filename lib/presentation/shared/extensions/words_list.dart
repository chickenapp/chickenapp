class WordList {
  late List<WordSingularPlural> wordList;
  String pluralOf(String word) {
    late final String result;
    final idx = wl.indexWhere((WordSingularPlural wlw) =>
        wlw.singular.toLowerCase() == word.toLowerCase());
    if (idx != -1) {
      result = wl[idx].plural.toLowerCase();
    } else {
      result = word.toLowerCase();
    }
    return result;
  }

  String singularOf(String word) {
    late final String result;
    final idx = wl.indexWhere((WordSingularPlural wlw) =>
        wlw.plural.toLowerCase() == word.toLowerCase());
    if (idx != -1) {
      result = wl[idx].singular.toLowerCase();
    } else {
      result = word.toLowerCase();
    }
    return result;
  }
}

class WordSingularPlural {
  WordSingularPlural({
    required this.singular,
    required this.plural,
  });
  String singular;
  String plural;
}

final List<WordSingularPlural> wl = [
  WordSingularPlural(singular: 'artículo', plural: 'artículos')
];
