import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/orders_controller.dart';
import 'package:repositoryflutter/src/routes/app_navigation.dart';

class PagesScreen extends GetView<HomeController> {
  const PagesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final OrdersController ordersCtrl = Get.put(OrdersController());
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: PageView(
          onPageChanged: (value) async => {
            if (value == 1)
              {await ordersCtrl.getActiveOrders(page: 1, size: 10)}
          },
          controller: controller.pageController,
          children: AppScreens.screens,
        ),
      ),
    );
  }
}
