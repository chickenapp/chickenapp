import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/home/widgets/index.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/customers_controller.dart';
import 'package:repositoryflutter/presentation/shared/widgets/index.dart';

class CustomerScreen extends GetView<CustomerController> {
  const CustomerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: HeaderApp(controller: controller.homeController, size: size),
        backgroundColor: Colors.white,
        body: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Stack(
              children: [
                ProductListHome(
                    controller: controller.homeController, size: size),
                BackDropHome(controller: controller.homeController),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
