import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/customers_controller.dart';

class CustomerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CustomerController>(
      () => CustomerController(),
    );
  }
}
