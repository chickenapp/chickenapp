// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/checkout/widgets/index.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/customers_controller.dart';
import 'package:repositoryflutter/presentation/shared/extensions/index.dart';
import '../shared/widgets/index.dart';

class CheckoutScreen extends GetView<HomeController> {
  const CheckoutScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final CustomerController customerController = Get.put(CustomerController());
    final Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: HeaderApp(
          controller: controller,
          size: size,
        ),
        backgroundColor: Colors.white,
        body: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Obx(
                    () => Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: controller.pedidosTramite.length.toString(),
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                          TextSpan(
                            text: ' artículo '.singularPlural(
                                controller.pedidosTramite.length),
                            style: const TextStyle(
                                color: Colors.black, fontSize: 20),
                          ),
                          const TextSpan(
                            text: ' tu cesta',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                      style: const TextStyle(fontSize: 30),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    height: 1,
                    color: Colors.black38,
                    width: size.width,
                  ),
                  Obx(
                    () => ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: controller.pedidosTramite.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ItemCart(
                          index: index,
                          size: size,
                          product: controller.pedidosTramite[index],
                          controller: controller,
                        );
                      },
                    ),
                  ),

                  //
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const Text(
                        'Total',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: size.width * .03,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 50),
                        child: Obx(
                          () => Text(
                            '${controller.getTotalOrder().toString()} €',
                            style: const TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                  ButtonsCart(
                    size: size,
                    controller: controller,
                    customerCtrl: customerController,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            primary: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          onPressed: () {
                            customerController.loadBottomSheetTypeUser(size);
                            controller.userFocus;
                          },
                          child: const Text(
                            'Tramitar pedido',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ButtonsCart extends StatelessWidget {
  ButtonsCart({
    Key? key,
    required this.size,
    required this.controller,
    this.customerCtrl,
  }) : super(key: key);

  final Size size;
  final HomeController controller;
  CustomerController? customerCtrl;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              elevation: 0,
              textStyle: const TextStyle(color: Colors.black38),
              primary: Colors.orange,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
            onPressed: () {
              Get.back();
            },
            child: const Text(
              'Seguir comprando',
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        SizedBox(
          width: size.width * .03,
        ),
        Expanded(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              elevation: 0,
              primary: Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
            onPressed: () {
              customerCtrl?.cleanVars();
              customerCtrl?.update();
              controller.clearProductsInCart();
              Get.back();
            },
            child: const Text(
              'Vaciar cesta',
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
