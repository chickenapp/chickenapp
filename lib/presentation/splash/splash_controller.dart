import 'package:get/get.dart';

class SplashController extends GetxController {
  RxDouble widthAnim = 0.0.obs;
  RxDouble heightAnim = 0.0.obs;
  RxDouble opacityAnim = 0.0.obs;
  var cont = 0;
  var nameComida = 'Pollos';
  final customers = [
    {
      "id": "b6e5ab41-87e9-4d03-a7c3-8b08d524d728",
      "name": "Aimee Porter",
      "phone": "843-660-4586x852",
      "email": "millerelizabeth@example.org",
      "active": true
    },
    {
      "id": "3811f891-c59c-4e76-b943-32cc201d0ffe",
      "name": "Alisha Santos",
      "phone": "001-295-778-5292",
      "email": "barrtamara@example.com",
      "active": true
    }
  ];
  SplashController();

  @override
  void onReady() async {
    opacityAnim(1.0);
    widthAnim(400.0);
    heightAnim(400.0);
    changeToHome();
    super.onReady();
  }

  changeToHome() async {
    await Future.delayed(const Duration(milliseconds: 1500), () {
      Get.offAndToNamed('/list');
    });
  }
}
