import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/splash/splash_controller.dart';

class SplashScreen extends GetView<SplashController> {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Obx(
          () => AnimatedContainer(
            // Use the properties stored in the State class.
            width: controller.widthAnim.value,
            height: controller.widthAnim.value,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
            ),
            // Define how long the animation should take.
            duration: const Duration(milliseconds: 1300),
            // Provide an optional curve to make the animation feel smoother.
            curve: Curves.bounceOut,
            child: Opacity(
              child: Hero(
                child: Image.asset('assets/images/logo.png'),
                tag: 'imageHero',
              ),
              opacity: controller.opacityAnim.value,
            ),
          ),
        ),
      ),
    );
  }
}
