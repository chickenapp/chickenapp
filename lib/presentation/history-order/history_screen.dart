import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/orders_controller.dart';

import '../shared/widgets/index.dart';

class HistoryScreen extends GetView<HomeController> {
  const HistoryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final OrdersController ordersCtrl = Get.put(OrdersController());
    final Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: HeaderApp(
          controller: controller,
          size: size,
        ),
        backgroundColor: Colors.white,
        body: Center(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Obx(
                () => Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(ordersCtrl.ordersToProcess.toString()),
                    const SizedBox(
                      height: 20,
                    ),
                    ...TextoTo(ordersCtrl),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            primary: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          onPressed: () async {
                            await ordersCtrl.getActiveOrders(page: 1, size: 1);
                          },
                          child: const Text(
                            'get orders 1',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            primary: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          onPressed: () async {
                            await ordersCtrl.getActiveOrders(page: 2, size: 1);
                          },
                          child: const Text(
                            'get orders 2',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  List<Widget> TextoTo(OrdersController ordersCtrl) {
    List<Widget> ll = [];
    if (ordersCtrl.listadoOrders.isEmpty) {
      ll.add(const SizedBox(
        child: Text('no hay órdenes para procesar'),
      ));
    } else {
      for (var registro in ordersCtrl.listadoOrders) {
        ll.add(Column(
          children: [
            const Divider(
              height: 1,
            ),
            Text('registro.id: ${registro.id}'),
            Text('registro.customerName: ${registro.customerName}'),
            Text('registro.orderStatus: ${registro.orderStatus}'),
            Text('registro.orderType: ${registro.orderType}'),
            const Divider(
              height: 2,
            ),
          ],
        ));
      }
    }
    return ll;
  }
}
