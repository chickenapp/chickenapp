import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/domain/models/customer.dart';
import 'package:repositoryflutter/presentation/home/home_controller.dart';

class AgendaController extends GetxController {
  AgendaController();
  final formKey = GlobalKey<FormState>();
  final HomeController homeController = Get.put(HomeController());

  RxList<Customer> customersAgenda = <Customer>[].obs;

  List<Customer> get usuarios {
    return customersAgenda;
  }
}
