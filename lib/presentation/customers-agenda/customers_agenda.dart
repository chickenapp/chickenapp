import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:repositoryflutter/presentation/shared/controllers/customers/customers_controller.dart';

import '../../domain/models/index.dart';

class CustomersContactListScreen extends GetView<CustomerController> {
  const CustomersContactListScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Scaffold(
          appBar: AppBar(
              title: Obx(() => Text(
                  'Agenda ${controller.listadoCustomers.length.toString()}')),
              actions: [
                IconButton(
                  icon: const Icon(Icons.search),
                  onPressed: () {
                    showSearch(
                        context: context,
                        delegate: DataSearch(controller),
                        query: '');
                  },
                ),
              ]),
          // appBar: HeaderApp(controller: controller, size: size),
          backgroundColor: Colors.white,
          body: CustomScrollView(
            slivers: [
              SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  return Obx(
                    () => Card(
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: ListTile(
                          title: Text(controller.listadoCustomers[index].name),
                          subtitle: Text(
                              controller.listadoCustomers[index].email ??
                                  'Email not registered'),
                          leading: Text(
                            index.toString(),
                          ),
                        ),
                      ),
                    ),
                  );
                }, childCount: controller.listadoCustomers.length),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class DataSearch extends SearchDelegate<String> {
  DataSearch(
    this.controller, {
    Key? key,
  });
  final CustomerController controller;

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.close),
        onPressed: () {
          Get.back();
          query = '';
        },
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.abc),
      onPressed: () {},
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final List<Customer> result = controller.listadoCustomers
        .where((user) => user.name.toLowerCase().contains(query.toLowerCase()))
        .toList();

    return ListView.builder(
      itemCount: result.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(10),
          child: Text(result[index].name),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final List<Customer> result = controller.listadoCustomers
        .where((user) => user.name.toLowerCase().contains(query.toLowerCase()))
        .toList();
    return ListView.builder(
      itemCount: result.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(10),
          child: Text(result[index].name),
        );
      },
    );
  }
}
